<?php

namespace Igsem\CMSAdminBundle\Controller;

use Igsem\CMSAdminBundle\Entity\Locale;
use Igsem\CMSAdminBundle\Form\Type\ConfigType;
use Igsem\CMSAdminBundle\Form\Type\LocaleType;
use Igsem\Libs\SiteConfig;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

/**
 * Locale controller.
 */
class ConfigController extends Controller
{
    /**
     * Lists all config options.
     *
     * @throws \LogicException
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $locales = $em->getRepository('IgsemCMSAdminBundle:Locale')->findAll();

        return $this->render('@IgsemCMSAdmin/Config/index.html.twig' , [
            'locales' => $locales ,
        ]);
    }

    /**
     * @param Locale $locale
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function editAction(Locale $locale , Request $request)
    {
        $config = $this->get('site_config.igsem')->getConfig($locale->getLocale());
        $form = $this->createForm(ConfigType::class , $config);
        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {
            $newConfig = $form->getData();
            if ($newConfig['site_logo'] instanceof UploadedFile) {
                $file = $newConfig['site_logo'];
                $fileName = md5(uniqid('config' , true)) . '.' . $file->guessExtension();
                $file->move(
                    $this->getParameter('config_dir') ,
                    $fileName
                );
                $newConfig['site_logo'] = $this->getParameter('config_dir_assets') . '/' . $fileName;
                $lastLogo = $this->get('kernel')->getRootDir() . '/../web' . $config['site_logo'];
                if (is_file($lastLogo)) {
                    unlink($lastLogo);
                }
            } else {
                unset($newConfig['site_logo']);
            }

            $this->get('site_config.igsem')->saveOptions($locale , $newConfig);

            return $this->redirectToRoute('config_index');
        }

        return $this->render('@IgsemCMSAdmin/Config/edit.html.twig' , [
            'form'   => $form->createView() ,
            'locale' => $locale ,
        ]);
    }
}

<?php

namespace Igsem\CMSAdminBundle\Controller;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Igsem\CMSAdminBundle\Entity\User;
use Igsem\CMSAdminBundle\Form\Type\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * Class SecurityController.
 */
class SecurityController extends Controller
{
    /**
     * @return Response
     */
    public function loginAction()
    {
        $authenticationUtils = $this->get('security.authentication_utils');
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('IgsemCMSAdminBundle:Security:login.html.twig' , [
            'last_username' => $lastUsername ,
            'error'         => $error ,
        ]);
    }

    /**
     * @return Response
     *
     * @throws \InvalidArgumentException
     */
    public function logoutAction()
    {
        return new Response();
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @throws \LogicException
     */
    public function registerAction(Request $request)
    {
        /** @var User $user */
        $user = new User();
        $user->setRoles(['ROLE_AUTHOR']);
        $user->setActive(false);
        $form = $this->createForm(UserType::class , $user);

        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {
            $password = $user->getPassword();
            $user->setPassword($this->get('security.password_encoder')->encodePassword($user , $password));
            $this->getDoctrine()->getManager()->persist($user);
            try {
                $this->getDoctrine()->getManager()->flush();
                $this->sendRegistrationEmail($user , $request->getLocale());

                return $this->redirectToRoute('igsem_cms_blog_admin_homepage');
            } catch (UniqueConstraintViolationException $e) {
                $form->addError(new FormError('Pouzivatel s tymto usernamemom uz existuje'));
            }
        }

        return $this->render('IgsemCMSAdminBundle:Security:register.html.twig' , [
            'form' => $form->createView() ,
        ]);
    }

    /**
     * Manual authentication.
     *
     * @param User $user
     *
     * @throws \InvalidArgumentException
     */
    private function authenticateUser(User $user)
    {
        $providerKey = 'main'; // your firewall name
        $token = new UsernamePasswordToken($user , null , $providerKey , $user->getRoles());

        $this->get('security.token_storage')->setToken($token);
    }

    /**
     * @param User   $user
     *
     * @param string $locale
     */
    private function sendRegistrationEmail(User $user , string $locale)
    {
        try {
            $html = $this->renderView('@IgsemCMSAdmin/Email/register.' . $locale . '.html.twig' , ['user' => $user]);
        } catch (\Exception $e) {
            $html = $this->renderView('@IgsemCMSAdmin/Email/register.en.html.twig' , ['user' => $user]);
        }

        $message = new \Swift_Message();
        $message->setTo($user->getEmail())
                ->setFrom($this->getParameter('mailer_from'))
                ->setSubject($this->get('translator.default')->trans('email.subject.user_registration',[],'messages'))
                ->setBody($html , 'text/html');

        $this->get('mailer')->send($message);
    }
}

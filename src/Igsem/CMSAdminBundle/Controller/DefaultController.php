<?php

namespace Igsem\CMSAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class DefaultController
 *
 * @package Igsem\CMSAdminBundle\Controller
 */
class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('IgsemCMSAdminBundle:Default:index.html.twig');
    }
}

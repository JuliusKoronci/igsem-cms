<?php

namespace Igsem\CMSAdminBundle\Controller;

use Igsem\CMSAdminBundle\Entity\Locale;
use Igsem\CMSAdminBundle\Form\Type\LocaleType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Locale controller.
 */
class LocaleController extends Controller
{
    /**
     * Lists all locale entities.
     *
     * @throws \LogicException
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $locales = $em->getRepository('IgsemCMSAdminBundle:Locale')->findAll();

        return $this->render('@IgsemCMSAdmin/Locale/index.html.twig' , [
            'locales' => $locales ,
        ]);
    }

    /**
     * Creates a new locale entity.
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function newAction(Request $request)
    {
        $locale = new Locale();
        $form = $this->createForm(LocaleType::class , $locale);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if ($locale->getDefault()) {
                $em->getRepository('IgsemCMSAdminBundle:Locale')->clearDefault();
            }
            $em->persist($locale);
            $em->flush();

            return $this->redirectToRoute('locale_show' , ['id' => $locale->getId()]);
        }

        return $this->render('@IgsemCMSAdmin/Locale/new.html.twig' , [
            'locale' => $locale ,
            'form'   => $form->createView() ,
        ]);
    }

    /**
     * Finds and displays a locale entity.
     *
     * @param Locale $locale
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Locale $locale)
    {
        $deleteForm = $this->createDeleteForm($locale);

        return $this->render('@IgsemCMSAdmin/Locale/show.html.twig' , [
            'locale'      => $locale ,
            'delete_form' => $deleteForm->createView() ,
        ]);
    }

    /**
     * Displays a form to edit an existing locale entity.
     *
     * @param Request $request
     * @param Locale  $locale
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function editAction(Request $request , Locale $locale)
    {
        $deleteForm = $this->createDeleteForm($locale);
        $editForm = $this->createForm(LocaleType::class , $locale);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            if ($locale->getDefault()) {
                $this->getDoctrine()->getManager()->getRepository('IgsemCMSAdminBundle:Locale')->clearDefault();
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('locale_edit' , ['id' => $locale->getId()]);
        }

        return $this->render('@IgsemCMSAdmin/Locale/edit.html.twig' , [
            'locale'      => $locale ,
            'edit_form'   => $editForm->createView() ,
            'delete_form' => $deleteForm->createView() ,
        ]);
    }

    /**
     * Deletes a locale entity.
     *
     * @param Request $request
     * @param Locale  $locale
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \LogicException
     */
    public function deleteAction(Request $request , Locale $locale)
    {
        $form = $this->createDeleteForm($locale);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($locale);
            $em->flush();
        }

        return $this->redirectToRoute('locale_index');
    }

    /**
     * Creates a form to delete a locale entity.
     *
     * @param Locale $locale The locale entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Locale $locale)
    {
        return $this->createFormBuilder()
                    ->setAction($this->generateUrl('locale_delete' , ['id' => $locale->getId()]))
                    ->setMethod('DELETE')
                    ->getForm();
    }
}

<?php

namespace Igsem\CMSAdminBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Igsem\Libs\SiteConfig;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class ConfigType
 *
 * @package Igsem\CMSAdminBundle\Form\Type
 */
class ConfigType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder , array $options)
    {
        $options = SiteConfig::getOptions();
        foreach ($options as $key => $option) {
            if ($option === 'text') {
                $builder->add($key , TextType::class , [
                    'required' => false,
                ]);
            }

            if ($option === 'upload') {
                $builder->add($key , FileType::class , [
                    'data_class' => null ,
                    'required' => false,
                ]);
            }
        }
        $builder->add('save' , SubmitType::class , [
            'label' => 'form.config.save' ,
        ]);
    }


    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'igsem_cmsadminbundle_site_config';
    }
}

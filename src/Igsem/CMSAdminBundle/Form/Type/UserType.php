<?php

namespace Igsem\CMSAdminBundle\Form\Type;

use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\IsTrue as RecaptchaTrue;

/**
 * Class UserType.
 */
class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     */
    public function buildForm(FormBuilderInterface $builder , array $options)
    {
        $builder
            ->add('email' , null , [
                'attr' => [
                    'placeholder' => 'form.user.email' ,
                ] ,
            ])
            ->add('nick' , null , [
                'attr' => [
                    'placeholder' => 'form.user.nick' ,
                ] ,
            ])
            ->add('password' , RepeatedType::class , [
                'type'            => PasswordType::class ,
                'invalid_message' => 'The password fields must match.' ,
                'options'         => ['attr' => ['class' => 'password-field']] ,
                'required'        => true ,
                'first_options'   => [
                    'label' => 'Password' ,
                    'attr'  => [
                        'class'       => 'form-control' ,
                        'placeholder' => 'form.user.password' ,
                    ] ,
                ] ,
                'second_options'  => [
                    'label' => 'Repeat Password' ,
                    'attr'  => [
                        'class'       => 'form-control' ,
                        'placeholder' => 'form.user.password_repeat' ,
                    ] ,
                ] ,

            ])
            ->add('surname' , null , [
                'attr' => [
                    'placeholder' => 'form.user.surname' ,
                ] ,
            ])
            ->add('name' , null , [
                'attr' => [
                    'placeholder' => 'form.user.name' ,
                ] ,
            ])
            ->add('recaptcha' , EWZRecaptchaType::class , [
                'mapped'      => false ,
                'constraints' => [new RecaptchaTrue()] ,
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Igsem\CMSAdminBundle\Entity\User' ,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'igsem_cmsadminbundle_user';
    }
}

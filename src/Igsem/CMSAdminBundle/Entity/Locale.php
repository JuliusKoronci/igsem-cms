<?php

namespace Igsem\CMSAdminBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Igsem\CMSBlogBundle\Entity\Article;

/**
 * Locale.
 *
 * @ORM\Table(name="locale")
 * @ORM\Entity(repositoryClass="Igsem\CMSAdminBundle\Repository\LocaleRepository")
 */
class Locale
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="locale", type="string", length=4, unique=true)
     */
    private $locale;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, unique=true)
     */
    private $title;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_default", type="boolean")
     */
    private $default;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Igsem\CMSBlogBundle\Entity\Article", mappedBy="locale", cascade={"remove"})
     */
    private $articles;

    /**
     * @var SiteConfig[]
     *
     * @ORM\OneToMany(targetEntity="Igsem\CMSAdminBundle\Entity\SiteConfig", mappedBy="locale", cascade={"remove"})
     */
    private $siteConfigs;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->articles = new ArrayCollection();
        $this->siteConfigs = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set locale.
     *
     * @param string $locale
     *
     * @return Locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get locale.
     *
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Locale
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set default.
     *
     * @param bool $default
     *
     * @return Locale
     */
    public function setDefault($default)
    {
        $this->default = $default;

        return $this;
    }

    /**
     * Get default.
     *
     * @return bool
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * Add article.
     *
     * @param Article $article
     *
     * @return Locale
     */
    public function addArticle(Article $article)
    {
        $this->articles[] = $article;

        return $this;
    }

    /**
     * Remove article.
     *
     * @param Article $article
     */
    public function removeArticle(Article $article)
    {
        $this->articles->removeElement($article);
    }

    /**
     * Get articles.
     *
     * @return Collection
     */
    public function getArticles()
    {
        return $this->articles;
    }

    public function __toString()
    {
        return $this->locale;
    }

    /**
     * Add siteConfig
     *
     * @param SiteConfig $siteConfig
     *
     * @return Locale
     */
    public function addSiteConfig(SiteConfig $siteConfig)
    {
        $this->siteConfigs[] = $siteConfig;

        return $this;
    }

    /**
     * Remove siteConfig
     *
     * @param SiteConfig $siteConfig
     */
    public function removeSiteConfig(SiteConfig $siteConfig)
    {
        $this->siteConfigs->removeElement($siteConfig);
    }

    /**
     * Get siteConfigs
     *
     * @return Collection
     */
    public function getSiteConfigs()
    {
        return $this->siteConfigs;
    }
}

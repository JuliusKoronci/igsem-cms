<?php
/**
 * Created by PhpStorm.
 * User: websolutions
 * Date: 12/5/16
 * Time: 6:34 PM
 */

namespace Igsem\Libs;

use Symfony\Component\Cache\Adapter\AbstractAdapter;
use Symfony\Component\Cache\Adapter\TagAwareAdapter;

/**
 * Class CacheService
 *
 * @package Igsem\CMSBlogBundle\Services
 */
class CacheService
{
    /** Tag used to invalidate all articles */
    const CACHE_TAG_ARTICLES = 'articles';

    /** Tag used to invalidate all articles held in lists */
    const CACHE_TAG_ARTICLES_LISTS = 'articles_lists';

    /** Prefix  for all single articles */
    const CACHE_PREFIX_ARTICLE = 'article';
    /** @var TagAwareAdapter */
    private $cache;

    /**
     * CacheService constructor.
     *
     * @param AbstractAdapter $adapter
     */
    public function __construct(AbstractAdapter $adapter)
    {
        $this->cache = new TagAwareAdapter(
            $adapter
        );
    }

    /**
     * @param string $articleSlug
     * @param mixed  $values
     *
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function createArticleItem(string $articleSlug , $values)
    {
        $key = self::CACHE_PREFIX_ARTICLE . '-' . $articleSlug;

        $this->createNewItem($key , $values , [self::CACHE_TAG_ARTICLES , $articleSlug]);
    }

    /**
     * @param string $articleSlug
     *
     * @return bool|mixed
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getArticleItem(string $articleSlug)
    {
        $key = self::CACHE_PREFIX_ARTICLE . '-' . $articleSlug;

        return $this->getCacheItemValue($key);
    }

    /**
     * @param int $articleId
     *
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function invalidateArticleItem(int $articleId)
    {
        $key = self::CACHE_PREFIX_ARTICLE . '-' . $articleId;

        $this->cache->deleteItem($key);
    }

    /**
     * Invalidate all articles in the system
     *
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function invalidateArticles()
    {
        $this->cache->invalidateTags([self::CACHE_TAG_ARTICLES]);
    }

    /**
     * @param string $key
     * @param mixed  $values
     *
     * @param array  $tags
     *
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function createNewItem(string $key , $values , array $tags = [])
    {
        $key = str_replace(['{' , '}' , '(' , ')' , '@' , ':'] , '--' , $key);
        $item = $this->cache->getItem($key);
        $item->set($values);
        $item->tag($tags);
        $this->cache->save($item);
    }

    /**
     * @param string $key
     *
     * @return bool|mixed
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getCacheItemValue(string $key)
    {
        $item = $this->cache->getItem($key);

        if ($item->isHit()) {
            return $item->get();
        }

        return false;
    }

    /**
     * @param array $tags
     *
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function invalidateTags(array $tags)
    {
        $this->cache->invalidateTags($tags);
    }

    /**
     * @return TagAwareAdapter
     */
    public function getCache(): TagAwareAdapter
    {
        return $this->cache;
    }
}
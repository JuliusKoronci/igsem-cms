<?php
/**
 * Created by PhpStorm.
 * User: websolutions
 * Date: 12/6/16
 * Time: 1:26 PM
 */

namespace Igsem\Libs;


use Doctrine\ORM\EntityManager;
use Igsem\CMSAdminBundle\Entity\Locale;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class SiteConfig
 *
 * @package Igsem\Libs
 */
class SiteConfig
{
    /** Tag for cache invalidation */
    const SITE_CONFIG_TAG = 'system.site.config';

    /** Cache Key */
    const SITE_CONFIG_CACHE_KEY = 'system.site.config.cache_key';

    /** fallback for missing translations */
    const DEFAULT_LOCALE = 'en';

    /** @var string */
    private $locale;

    /** @var CacheService */
    private $cache;

    /** @var EntityManager */
    private $em;

    /** @var array */
    private $config = [];

    /**
     * SiteConfig constructor.
     *
     * @param RequestStack  $stack
     * @param CacheService  $cache
     * @param EntityManager $em
     *
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function __construct(RequestStack $stack , CacheService $cache , EntityManager $em)
    {
        $request = $stack->getCurrentRequest();
        $this->locale = $request ? $request->getLocale() : 'en';
        $this->cache = $cache;
        $this->em = $em;

        if ($request && $request->get('clear_cache')) {
            $this->cache->invalidateTags([self::SITE_CONFIG_TAG]);
        }
        $cachedConfig = $this->cache->getCacheItemValue(self::SITE_CONFIG_CACHE_KEY);
        $this->config = $cachedConfig;
        if (false === $cachedConfig) {
            $this->config = $this->buildConfigArray();
        }
    }

    /**
     * @param $locale
     *
     * @return array|mixed
     */
    public function getConfig($locale)
    {
        if (!empty($this->config[$locale])) {
            return $this->config[$locale];
        }

        return [];
    }

    /**
     * @param string $key
     * @param bool   $missingText
     *
     * @return null|string
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     */
    public function getByKey($key , $missingText = true)
    {
        if (!empty($this->config[$this->locale][$key])) {
            return $this->config[$this->locale][$key];
        } else {
            if (!empty($this->config[self::DEFAULT_LOCALE][$key])) {
                return $this->config[self::DEFAULT_LOCALE][$key];
            }
            $locale = $this->em->getRepository('IgsemCMSAdminBundle:Locale')->findOneBy([
                'locale' => $this->locale ,
            ]);

            if (null !== $locale) {
                $exists = $this->em->getRepository('IgsemCMSAdminBundle:SiteConfig')->findOneBy([
                    'locale' => $locale ,
                    'code'   => $key ,
                ]);
                if (null === $exists) {
                    $siteConfig = new \Igsem\CMSAdminBundle\Entity\SiteConfig();
                    $siteConfig->setCode($key);
                    $siteConfig->setLocale($locale);

                    $this->em->persist($siteConfig);
                    $this->em->flush();
                }
            }
            if ($missingText) {
                return 'missing value';
            }

            return null;
        }
    }

    /**
     * @param Locale $locale
     * @param array  $options
     *
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveOptions(Locale $locale , array $options)
    {
        foreach ($options as $key => $option) {
            $configItem = $this->em->getRepository('IgsemCMSAdminBundle:SiteConfig')->findOneBy([
                'locale' => $locale ,
                'code'   => $key ,
            ]);
            if (null === $configItem) {
                $configItem = new \Igsem\CMSAdminBundle\Entity\SiteConfig();
                $configItem->setLocale($locale);
                $configItem->setCode($key);
            }

            $configItem->setValue($option);

            $this->em->persist($configItem);
        }

        $this->em->flush();

        $this->cache->invalidateTags([self::SITE_CONFIG_TAG]);
        $this->config = $this->buildConfigArray();
    }

    /**
     * @return array
     */
    public static function getOptions()
    {
        return [
            'site_title'       => 'text' ,
            'site_description' => 'text' ,
            'site_logo'        => 'upload' ,
            'ytb_link'         => 'text' ,
            'facebook_link'    => 'text' ,
            'twitter_link'     => 'text' ,
            'contact_email'    => 'text' ,
            'contact_phone'    => 'text' ,
            'contact_address'  => 'text' ,
        ];
    }

    /**
     * @return array
     * @throws \Psr\Cache\InvalidArgumentException
     */
    private function buildConfigArray()
    {
        $locales = $this->em->getRepository('IgsemCMSAdminBundle:Locale')->findAll();
        $config = [];
        foreach ($locales as $locale) {
            $config[$locale->getLocale()] = [];
            /** @var \Igsem\CMSAdminBundle\Entity\SiteConfig $siteConfig */
            foreach ($locale->getSiteConfigs() as $siteConfig) {
                $config[$locale->getLocale()][$siteConfig->getCode()] = $siteConfig->getValue();
            }
        }
        $this->cache->createNewItem(self::SITE_CONFIG_CACHE_KEY , $config , [self::SITE_CONFIG_TAG]);

        return $config;
    }

}
<?php

namespace Igsem\CMSBlogBundle\Form\Type;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ArticleType.
 */
class ArticleType extends AbstractType
{
    /** @var EntityManager */
    private $em;

    /**
     * ArticleType constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder , array $options)
    {
        $builder
            ->add('title' , null , [
                'label' => 'blog.form.article.title' ,
            ])
            ->add('body' , null , [
                'label' => 'blog.form.article.body' ,
                'attr'  => [
                    'class' => 'ckeditor' ,
                ] ,
            ])
            ->add('image' , FileType::class , [
                'label'      => 'blog.form.article.image' ,
                'data_class' => null ,
                'required'   => false ,
            ])
            ->add('enableDiscussion' , null , [
                'label' => 'blog.form.article.enable_discussion' ,
            ])
            ->add('locale' , null , [
                'label'       => 'blog.form.article.locale' ,
                'placeholder' => false ,
            ])
            ->add('tag_options' , TextType::class , [
                'label'  => 'blog.form.article.tags' ,
                'mapped' => false ,
                'attr'   => [
                    'class'        => 'selectize' ,
                    'data-options' => json_encode($this->getTags()) ,
                ] ,
            ])->add('featured' , null , [
                'label' => 'blog.form.article.featured' ,
            ])->add('main' , null , [
                'label' => 'blog.form.article.main' ,
            ])
            ->add('submit' , SubmitType::class , [
                'label' => 'blog.form.article.save' ,
                'attr'  => [
                    'class' => 'btn btn-success' ,
                ] ,
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Igsem\CMSBlogBundle\Entity\Article' ,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'igsem_cmsblogbundle_article';
    }

    private function getTags()
    {
        return $this->em->getRepository('IgsemCMSBlogBundle:Tag')->getSelectOptions();
    }
}

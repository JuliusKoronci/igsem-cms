<?php
/**
 * Created by PhpStorm.
 * User: websolutions
 * Date: 11/27/16
 * Time: 5:06 PM.
 */

namespace Igsem\CMSBlogBundle\Services;

use Doctrine\ORM\EntityManager;
use Igsem\CMSAdminBundle\Entity\Locale;
use Igsem\CMSBlogBundle\Entity\Article;
use Igsem\Libs\CacheService;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class Featured.
 */
class Featured
{
    /** @var EntityManager */
    private $entityManager;

    /** @var CacheService */
    private $cache;

    /** @var Locale */
    private $locale;

    /** @var bool|Article */
    private $main = false;

    /**
     * Featured constructor.
     *
     * @param EntityManager $entityManager
     * @param CacheService  $cache
     *
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function __construct(RequestStack $stack , EntityManager $entityManager , CacheService $cache)
    {
        $this->entityManager = $entityManager;
        $this->cache = $cache;

        $request = $stack->getCurrentRequest();

        if ($request && $request->get('clear_cache')) {
            $this->cache->invalidateTags([CacheService::CACHE_TAG_ARTICLES]);
        }

        $locale = $request ? $request->getLocale() : 'en';

        $this->locale = $entityManager->getRepository('IgsemCMSAdminBundle:Locale')->findOneBy([
            'locale' => $locale ,
        ]);
    }

    /**
     * @return \Igsem\CMSBlogBundle\Entity\Article|null|object
     */
    public function getMainArticle()
    {
        if (false !== $this->main) {
            return $this->main;
        }

        $article = $this->entityManager->getRepository('IgsemCMSBlogBundle:Article')->findOneBy([
            'main' => true ,
        ]);

        $this->main = $article;

        return $article;
    }

    /**
     * @return array|\Igsem\CMSBlogBundle\Entity\Article[]
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getPopularArticles()
    {
        $key = 'featured-popular-' . $this->locale->getLocale();
        $cached = $this->cache->getCacheItemValue($key);
        if ($cached) {
            return $cached;
        }

        $return = $this->entityManager->getRepository('IgsemCMSBlogBundle:Article')->findBy(
            [
                'locale' => $this->locale ,
                'main'   => false ,
            ] ,
            [
                'popularity' => 'desc' ,
            ] , 3);

        $this->cache->createNewItem($key , $return , [CacheService::CACHE_TAG_ARTICLES]);

        return $return;
    }

    /**
     * @return array|\Igsem\CMSBlogBundle\Entity\Article[]
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getFeaturedArticles()
    {
        $key = 'featured-featured-' . $this->locale->getLocale();
        $cached = $this->cache->getCacheItemValue($key);
        if ($cached) {
            return $cached;
        }

        $return = $this->entityManager->getRepository('IgsemCMSBlogBundle:Article')->findBy(
            [
                'featured' => true ,
                'main'     => false ,
                'locale'   => $this->locale ,
            ] ,
            [
                'popularity' => 'desc' ,
            ] , 3);

        $this->cache->createNewItem($key , $return , [CacheService::CACHE_TAG_ARTICLES]);

        return $return;
    }

    public function getLocales()
    {
        return $this->entityManager->getRepository('IgsemCMSAdminBundle:Locale')->findAll();
    }

    /**
     * @return array|bool|mixed
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getAuthorArticles()
    {
        $key = 'featured-author';
        $cached = $this->cache->getCacheItemValue($key);
        if ($cached) {
            return $cached;
        }

        $return = $this->entityManager->getRepository('IgsemCMSBlogBundle:Article')->findAuthorArticles();

        $this->cache->createNewItem($key , $return , [CacheService::CACHE_TAG_ARTICLES]);

        return $return;
    }

    /**
     * @return array|\Igsem\CMSBlogBundle\Entity\Tag[]
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getTags()
    {
        $key = 'featured-tags';
        $cached = $this->cache->getCacheItemValue($key);
        if ($cached) {
            return $cached;
        }
        $return = $this->entityManager->getRepository('IgsemCMSBlogBundle:Tag')->findBy([] , ['count' => 'desc']);

        $this->cache->createNewItem($key , $return , [CacheService::CACHE_TAG_ARTICLES]);

        return $return;
    }

    /**
     * Call to regenerate all caches
     *
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function regenerateAfterCacheInvalidation()
    {
        $this->getAuthorArticles();
        $this->getFeaturedArticles();
        $this->getPopularArticles();
        $this->getTags();
    }
}

<?php

namespace Igsem\CMSBlogBundle\Controller;

use Igsem\CMSBlogBundle\Entity\Article;
use Igsem\Libs\CacheService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class DefaultController.
 */
class DefaultController extends Controller
{
    /**
     * @param Request $request
     *
     * @Cache(smaxage="10", public=true)
     *
     * @return Response
     * @throws \LogicException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function indexAction(Request $request)
    {
        $locale = $request->getLocale();
        $cacheKey = $locale . '--index-page';

        $cachedResponse = $this->get('cache.igsem')->getCacheItemValue($cacheKey);
        if ($cachedResponse) {
            return $cachedResponse;
        }


        $language = $this->getDoctrine()->getRepository('IgsemCMSAdminBundle:Locale')->findOneBy([
            'locale' => $locale ,
        ]);
        $articles = $this->getDoctrine()->getRepository('IgsemCMSBlogBundle:Article')->findBy(
            [
                'locale' => $language ,
                'main'   => false,
            ] ,
            [
                'createdAt' => 'desc' ,
                'rating'    => 'desc' ,
                'viewCount' => 'desc' ,
            ] ,
            15
        );

        $render = $this->render('IgsemCMSBlogBundle:Default:index.html.twig' , [
            'articles' => $articles ,
        ]);

        $this->get('cache.igsem')->createNewItem($cacheKey , $render , [CacheService::CACHE_TAG_ARTICLES]);

        return $render;
    }


    /**
     * @param $slug
     *
     * @Cache(smaxage="15", public=true)
     *
     * @return Response
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \LogicException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function articleAction($slug)
    {
        $article = $this->get('cache.igsem')->getArticleItem($slug);

        if (false === $article) {
            $article = $this->getDoctrine()->getRepository('IgsemCMSBlogBundle:Article')->findOneBy([
                'slug' => $slug ,
            ]);

            if (null === $article) {
                throw $this->createNotFoundException('Article not found');
            }
        }

        return $this->render('IgsemCMSBlogBundle:Default:article.html.twig' , [
            'article' => $article ,
        ]);
    }

    /**
     * @param Article $article
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Psr\Cache\InvalidArgumentException
     *
     * @throws \LogicException
     */
    public function articleViewAction(Article $article)
    {
        $article->setViewCount($article->getViewCount() + 1);

        $this->getDoctrine()->getManager()->persist($article);
        $this->getDoctrine()->getManager()->flush();

        $this->get('cache.igsem')->createArticleItem($article->getSlug() , $article);

        return $this->json([
            'count' => $article->getViewCount() ,
        ]);
    }

    /**
     * @param Article $article
     * @param bool    $like
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \LogicException
     */
    public function articleLikeAction(Article $article , $like)
    {
        if ($like) {
            $article->setLikes($article->getLikes() + 1);
        } else {
            $article->setDislikes($article->getDislikes() + 1);
        }

        $this->getDoctrine()->getManager()->persist($article);
        $this->getDoctrine()->getManager()->flush();


        $this->get('cache.igsem')->createArticleItem($article->getSlug() , $article);

        return $this->json([
            'likes'    => $article->getLikes() ,
            'dislikes' => $article->getDislikes() ,
        ]);
    }

    /**
     * @param string $slug
     *
     * @return Response
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \LogicException
     * @throws NotFoundHttpException
     */
    public function tagsAction(string $slug)
    {
        $cacheKey = '--tags-page-' . $slug;

        $cachedResponse = $this->get('cache.igsem')->getCacheItemValue($cacheKey);
        if ($cachedResponse) {
            return $cachedResponse;
        }
        $tag = $this->getDoctrine()->getRepository('IgsemCMSBlogBundle:Tag')->findOneBy([
            'slug' => $slug ,
        ]);

        if (null === $tag) {
            throw $this->createNotFoundException('Tag not found');
        }

        $render = $this->render('IgsemCMSBlogBundle:Default:tags.html.twig' , [
            'articles' => $this->getDoctrine()->getRepository('IgsemCMSBlogBundle:Article')->findByTag($tag , 10) ,
        ]);

        $this->get('cache.igsem')->createNewItem($cacheKey , $render , [CacheService::CACHE_TAG_ARTICLES]);

        return $render;
    }
}

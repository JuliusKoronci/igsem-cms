<?php

namespace Igsem\CMSBlogBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('@IgsemCMSBlog/Admin/Default/index.html.twig');
    }
}

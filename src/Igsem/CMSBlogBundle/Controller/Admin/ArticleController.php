<?php

namespace Igsem\CMSBlogBundle\Controller\Admin;

use Eventviva\ImageResize;
use Igsem\CMSBlogBundle\Entity\Article;
use Igsem\CMSBlogBundle\Entity\Tag;
use Igsem\CMSBlogBundle\Form\Type\ArticleType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ArticleController.
 */
class ArticleController extends Controller
{
    public function indexAction(Request $request)
    {
        $article = new Article();
        $article->setUser($this->getUser());
        $form = $this->createForm(ArticleType::class , $article);

        if (!$this->isGranted('ROLE_ADMIN')) {
            $form->remove('featured');
            $form->remove('main');
        }

        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {
            return $this->articleNewFormSuccess($article , $form);
        }


        return $this->render('@IgsemCMSBlog/Admin/Article/index.html.twig' , [
            'form'     => $form->createView() ,
            'articles' => $this->getDoctrine()->getRepository('IgsemCMSBlogBundle:Article')->findBy(
                [
                    'user' => $this->getUser() ,
                ] ,
                [
                    'createdAt' => 'desc' ,
                ] ,
                10
            ) ,
        ]);
    }

    public function editAction(Request $request , Article $article)
    {
        $origImage = $article->getImage();
        $origThumb = $article->getThumb();
        $tags = $article->getTags();

        $tag_options = [];
        foreach ($tags as $t) {
            $tag_options[] = $t->getTitle();
        }
        $tag_options = implode(',' , $tag_options);

        $form = $this->createForm(ArticleType::class , $article);

        if (!$this->isGranted('ROLE_ADMIN')) {
            $form->remove('featured');
            $form->remove('main');
        }

        $form->get('tag_options')->setData($tag_options);
        $form->setData([
            'tag_options' => $tag_options ,
        ]);

        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {
            return $this->articleEditFormSuccess($article , $form , $origImage , $origThumb);
        }

        return $this->render('@IgsemCMSBlog/Admin/Article/edit.html.twig' , [
            'form'     => $form->createView() ,
            'articles' => $this->getDoctrine()->getRepository('IgsemCMSBlogBundle:Article')->findBy([
                'user' => $this->getUser() ,
            ]) ,
        ]);
    }

    public function deleteAction(Article $article)
    {
        if (!$this->isGranted('ROLE_ADMIN') || $this->getUser()->getId() !== $article->getUser()->getId()) {
            $this->addFlash('error' , $this->get('translator.default')->trans('blog.article.not_allowed'));

            return $this->redirectToRoute('igsem_cms_blog_admin_articles');
        }

        if (file_exists($this->getParameter('image_dir') . '/' . $article->getImage())) {
            unlink($this->getParameter('image_dir') . '/' . $article->getImage());
            unlink($this->getParameter('image_dir') . '/' . $article->getThumb());
        }

        $this->getDoctrine()->getManager()->remove($article);
        $this->getDoctrine()->getManager()->flush();
        $this->addFlash('success' , $this->get('translator.default')->trans('blog.article.deleted'));

        return $this->redirectToRoute('igsem_cms_blog_admin_articles');
    }

    private function handleArticleTags($tags , Article $article)
    {
        foreach ($article->getTags() as $tag) {
            $article->removeTag($tag);
        }

        $tags = explode(',' , $tags);
        foreach ($tags as $tag) {
            $t = $this->getDoctrine()->getRepository('IgsemCMSBlogBundle:Tag')->findOneBy([
                'code' => strtolower($tag) ,
            ]);

            if (null === $t) {
                $t = new Tag();
                $t->setTitle($tag);
            } else {
                $t->setCount($t->getCount() + 1);
            }
            $this->getDoctrine()->getManager()->persist($t);

            $article->addTag($t);
        }
    }

    /**
     * @param Article $article
     * @param Form    $form
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \OutOfBoundsException
     * @throws \LogicException
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileException
     * @throws \Exception
     */
    private function articleNewFormSuccess(Article $article , Form $form)
    {
        /** @var File $file */
        $file = $article->getImage();
        $tags = $form->get('tag_options')->getData();
        $this->handleArticleTags($tags , $article);
        if ($file) {
            $fileName = md5(uniqid('article' , true)) . '.' . $file->guessExtension();
            $file->move(
                $this->getParameter('image_dir') ,
                $fileName
            );
            $thumbName = 'thumb_' . $fileName;
            $thumb = new ImageResize($this->getParameter('image_dir') . '/' . $fileName);
            $thumb->resizeToWidth(250);
            $thumb->save($this->getParameter('image_dir') . '/' . $thumbName);

            $article->setImage($fileName);
            $article->setThumb($thumbName);
        }
        $article->setPlainBody(strip_tags($article->getBody()));
        $this->getDoctrine()->getManager()->persist($article);
        $this->getDoctrine()->getManager()->flush();
        $this->addFlash('success' , $this->get('translator.default')->trans('blog.article.created'));

        $this->fixArticleCache($article);

        return $this->redirectToRoute('igsem_cms_blog_admin_articles');
    }

    /**
     * @param Article $article
     * @param Form    $form
     * @param         $origImage
     * @param         $origThumb
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \OutOfBoundsException
     * @throws \LogicException
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileException
     * @throws \Exception
     */
    private function articleEditFormSuccess(Article $article , Form $form , $origImage , $origThumb)
    {
        $article->setPlainBody(strip_tags($article->getBody()));

        $tags = $form->get('tag_options')->getData();
        $this->handleArticleTags($tags , $article);

        if (true === $form->get('main')->getData()) {
            $this->getDoctrine()->getRepository('IgsemCMSBlogBundle:Article')->clearMain();
        }

        /** @var File $file */
        $file = $article->getImage();
        if ($file) {
            $fileName = md5(uniqid('article' , true)) . '.' . $file->guessExtension();
            $file->move(
                $this->getParameter('image_dir') ,
                $fileName
            );
            $thumbName = 'thumb_' . $fileName;
            $thumb = new ImageResize($this->getParameter('image_dir') . '/' . $fileName);
            $thumb->resizeToWidth(250);
            $thumb->save($this->getParameter('image_dir') . '/' . $thumbName);

            $article->setImage($fileName);
            $article->setThumb($thumbName);
            if (file_exists($this->getParameter('image_dir') . '/' . $origImage)) {
                unlink($this->getParameter('image_dir') . '/' . $origImage);
                unlink($this->getParameter('image_dir') . '/' . $origThumb);
            }
        } else {
            $article->setImage($origImage);
        }

        $this->getDoctrine()->getManager()->persist($article);
        $this->getDoctrine()->getManager()->flush();
        $this->addFlash('success' , $this->get('translator.default')->trans('blog.article.updated'));

        $this->fixArticleCache($article);

        return $this->redirectToRoute('igsem_cms_blog_admin_articles');
    }

    /**
     * @param Article $article
     *
     * @throws \Psr\Cache\InvalidArgumentException
     */
    private function fixArticleCache(Article $article)
    {
        $this->get('cache.igsem')->invalidateArticles();
        $this->get('cache.igsem')->createArticleItem($article->getSlug() , $article);
        $this->get('featured')->regenerateAfterCacheInvalidation();
    }
}

<?php

namespace Igsem\CMSBlogBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Igsem\CMSAdminBundle\Entity\Locale;
use Igsem\CMSAdminBundle\Entity\User;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Article.
 *
 * @ORM\Table(name="article",indexes={@ORM\Index(name="popularity_idx", columns={"popularity"})})
 * @ORM\Entity(repositoryClass="Igsem\CMSBlogBundle\Repository\ArticleRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Article
{
    use TimestampableEntity;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank(message="blog.article.not_blank.title")
     */
    private $title;

    /**
     * @var string
     * @Gedmo\Slug(fields={"title", "createdAt"})
     * @ORM\Column(name="slug", type="string", length=255)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text")
     * @Assert\NotBlank(message="blog.article.not_blank.body")
     */
    private $body;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Assert\Image()
     */
    private $image;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $thumb;

    /**
     * @var string
     *
     * @ORM\Column(name="plain_body", type="text")
     */
    private $plainBody;

    /**
     * @var bool
     *
     * @ORM\Column(name="enable_discussion", type="boolean")
     */
    private $enableDiscussion = true;

    /**
     * @var bool
     *
     * @ORM\Column(name="view_count", type="integer")
     */
    private $viewCount = 0;

    /**
     * @var bool
     *
     * @ORM\Column(name="rating", type="integer")
     */
    private $rating = 0;
    /**
     * @var bool
     *
     * @ORM\Column(name="likes", type="integer")
     */
    private $likes = 0;

    /**
     * @var bool
     *
     * @ORM\Column(name="dislikes", type="integer")
     */
    private $dislikes = 0;

    /**
     * @var bool
     *
     * @ORM\Column(name="popularity", type="integer")
     */
    private $popularity = 0;

    /**
     * @var bool
     *
     * @ORM\Column(name="featured", type="boolean")
     */
    private $featured = false;
    /**
     * @var bool
     *
     * @ORM\Column(name="main", type="boolean")
     */
    private $main = false;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Igsem\CMSAdminBundle\Entity\User", inversedBy="articles")
     */
    private $user;
    /**
     * @var Locale
     *
     * @ORM\ManyToOne(targetEntity="Igsem\CMSAdminBundle\Entity\Locale", inversedBy="articles")
     * @Assert\NotBlank(message="blog.article.not_blank.locale")
     */
    private $locale;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Igsem\CMSBlogBundle\Entity\Tag", inversedBy="articles", cascade={"remove"})
     * @ORM\JoinTable(name="article_tags")
     */
    private $tags;

    /**
     * Article constructor.
     */
    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Article
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set body.
     *
     * @param string $body
     *
     * @return Article
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body.
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set plainBody.
     *
     * @param string $plainBody
     *
     * @return Article
     */
    public function setPlainBody($plainBody)
    {
        $this->plainBody = $plainBody;

        return $this;
    }

    /**
     * Get plainBody.
     *
     * @return string
     */
    public function getPlainBody()
    {
        return $this->plainBody;
    }

    /**
     * Set enableDiscussion.
     *
     * @param bool $enableDiscussion
     *
     * @return Article
     */
    public function setEnableDiscussion($enableDiscussion)
    {
        $this->enableDiscussion = $enableDiscussion;

        return $this;
    }

    /**
     * Get enableDiscussion.
     *
     * @return bool
     */
    public function getEnableDiscussion()
    {
        return $this->enableDiscussion;
    }

    /**
     * Set user.
     *
     * @param User $user
     *
     * @return Article
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set locale.
     *
     * @param Locale $locale
     *
     * @return Article
     */
    public function setLocale(Locale $locale = null)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get locale.
     *
     * @return Locale
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set viewCount.
     *
     * @param int $viewCount
     *
     * @return Article
     */
    public function setViewCount($viewCount)
    {
        $this->viewCount = $viewCount;

        return $this;
    }

    /**
     * Get viewCount.
     *
     * @return int
     */
    public function getViewCount()
    {
        return $this->viewCount;
    }

    /**
     * Set slug.
     *
     * @param string $slug
     *
     * @return Article
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set rating.
     *
     * @param int $rating
     *
     * @return Article
     */
    public function setRating($rating)
    {
        $this->rating = $rating > 10 ? 10 : $rating;

        return $this;
    }

    /**
     * Get rating.
     *
     * @return int
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set image.
     *
     * @param string $image
     *
     * @return Article
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set thumb.
     *
     * @param string $thumb
     *
     * @return Article
     */
    public function setThumb($thumb)
    {
        $this->thumb = $thumb;

        return $this;
    }

    /**
     * Get thumb.
     *
     * @return string
     */
    public function getThumb()
    {
        return $this->thumb;
    }

    /**
     * Add tag.
     *
     * @param Tag $tag
     *
     * @return Article
     */
    public function addTag(Tag $tag)
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * Remove tag.
     *
     * @param Tag $tag
     */
    public function removeTag(Tag $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * Get tags.
     *
     * @return Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set likes.
     *
     * @param int $likes
     *
     * @return Article
     */
    public function setLikes($likes)
    {
        $this->likes = $likes;

        return $this;
    }

    /**
     * Get likes.
     *
     * @return int
     */
    public function getLikes()
    {
        return $this->likes;
    }

    /**
     * Set dislikes.
     *
     * @param int $dislikes
     *
     * @return Article
     */
    public function setDislikes($dislikes)
    {
        $this->dislikes = $dislikes;

        return $this;
    }

    /**
     * Get dislikes.
     *
     * @return int
     */
    public function getDislikes()
    {
        return $this->dislikes;
    }

    /**
     * Set popularity.
     *
     * @ORM\PreUpdate
     *
     * @return Article
     */
    public function setPopularity()
    {
        $sum = $this->likes + $this->dislikes + $this->getRating() + $this->viewCount;
        if ($this->rating > 0) {
            $sum *= $this->rating;
        }
        $this->popularity = $sum;

        return $this;
    }

    /**
     * Get popularity.
     *
     * @return int
     */
    public function getPopularity()
    {
        return $this->popularity;
    }

    /**
     * Set featured.
     *
     * @param bool $featured
     *
     * @return Article
     */
    public function setFeatured($featured)
    {
        $this->featured = $featured;

        return $this;
    }

    /**
     * Get featured.
     *
     * @return bool
     */
    public function getFeatured()
    {
        return $this->featured;
    }

    /**
     * Set main
     *
     * @param boolean $main
     *
     * @return Article
     */
    public function setMain($main)
    {
        $this->main = $main;

        return $this;
    }

    /**
     * Get main
     *
     * @return boolean
     */
    public function getMain()
    {
        return $this->main;
    }
}

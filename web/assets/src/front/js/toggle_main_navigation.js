$('.toggle-main-navigation').on('click', function () {
    var nav = $('#main_navigation');

    if (nav.hasClass('open')) {
        nav.removeClass('open').slideUp(500);
    } else {
        nav.addClass('open').slideDown(500);
    }
});
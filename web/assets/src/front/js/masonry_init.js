var $masonry = $('#masonry').masonry({
    itemSelector: '.col-lg-4'
});

$masonry.imagesLoaded().progress(function () {
    $masonry.masonry('layout');
});
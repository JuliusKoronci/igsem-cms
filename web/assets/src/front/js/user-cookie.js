$('.check-cookie').on('click', function () {
    var cKey = $(this).attr('data-coockie-key');

    if (Cookies.get(cKey)) {

    } else {
        Cookies.set(cKey, true);
        var $that = $(this);
        if ($that.hasClass('likes')) {
            $.ajax({
                url: $that.attr('href'),
                success: function (response) {
                    $that.find('span').html(response.likes);
                }
            });
        }
        if ($that.hasClass('dislikes')) {
            $.ajax({
                url: $that.attr('href'),
                success: function (response) {
                    $that.find('span').html(response.dislikes);
                }
            });
        }

        $(this).addClass('in-cookie');
    }

    return false;
});


$(document).ready(function () {
    $('.check-cookie').each(function () {
        var cKey = $(this).attr('data-coockie-key');
        if (Cookies.get(cKey)) {
            $(this).addClass('in-cookie');
        }
    })
});

articleView = function () {
    var key = 'article_view_count' + $('#article_single').attr('data-article-id');
    if (Cookies.get(key)) {
    } else {
        Cookies.set(key, true);
        $.ajax($('#article_single').attr('data-article-view'));
    }
};
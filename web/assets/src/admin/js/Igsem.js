$('#igsem-navbar-toggler').on('click', function () {
    $('#navbar').toggleClass('igsem-hidden-navbar');
});

$('.confirm_message').on('click', function (e) {
    return confirm($(this).attr('data-message'));
});

var selectize = $('.selectize').selectize({
    delimiter: ',',
    persist: false,
    valueField: 'value',
    labelField: 'value',
    searchField: 'value',
    options: [],
    create: function (input) {
        return {
            value: input,
            text: input
        }
    },
    render: {
        option: function (item, escape) {
            return '<div>' + escape(item.value) + '</div>';
        }
    },
    load: function (query, callback) {
        if (!query.length) return callback();
        var options = JSON.parse($('.selectize').attr('data-options'));
        callback(options);

    }
});

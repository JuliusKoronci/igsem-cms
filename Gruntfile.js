module.exports = function (grunt) {
    grunt.initConfig({
        distFolder: 'web/assets/dist',
        pkg: grunt.file.readJSON('package.json'),
        cssmin: {
            bundled: {
                src: '<%= distFolder %>/bundled.css',
                dest: '<%= distFolder %>/bundled.min.css'
            },
            bundled_front: {
                src: '<%= distFolder %>/bundled_front.css',
                dest: '<%= distFolder %>/bundled_front.min.css'
            }
        },
        uglify: {
            js: {
                files: {
                    '<%= distFolder %>/bundled.min.js': ['<%= distFolder %>/bundled.js'],
                    '<%= distFolder %>/bundled_front.min.js': ['<%= distFolder %>/bundled_front.js']
                }
            }
        },
        concat: {
            options: {
                stripBanners: true
            },
            css: {
                src: [
                    'node_modules/bootstrap/dist/css/bootstrap.css',
                    'node_modules/toastr/build/toastr.css',
                    'node_modules/selectize/dist/css/selectize.bootstrap3.css',
                    'web/assets/src/admin/css/*',
                ],
                dest: '<%= distFolder %>/bundled.css'
            },
            js: {
                src: [
                    'node_modules/jquery/dist/jquery.js',
                    'node_modules/tether/dist/js/tether.js',
                    'node_modules/toastr/build/toastr.min.js',
                    'node_modules/bootstrap/dist/js/bootstrap.js',
                    'node_modules/selectize/dist/js/standalone/selectize.js',
                    'web/assets/src/admin/js/*'
                ],
                dest: '<%= distFolder %>/bundled.js'
            },
            css_front: {
                src: [
                    'web/assets/src/front/blog/bootstrap/css/bootstrap.css',
                    'web/assets/src/front/css/*',
                ],
                dest: '<%= distFolder %>/bundled_front.css'
            },
            js_front: {
                src: [
                    'node_modules/jquery/dist/jquery.js',
                    'node_modules/imagesloaded/imagesloaded.pkgd.js',
                    'node_modules/masonry-layout/dist/masonry.pkgd.js',
                    'web/assets/src/front/js/*'
                ],
                dest: '<%= distFolder %>/bundled_front.js'
            }
        },
        sass: {
            dist: {
                files: [
                    {
                        expand: true,
                        cwd: 'web/assets/src/admin/scss',
                        src: ['*.scss'],
                        dest: 'web/assets/src/admin/css',
                        ext: '.css'
                    },
                    {
                        expand: true,
                        cwd: 'web/assets/src/front/scss',
                        src: ['*.scss'],
                        dest: 'web/assets/src/front/css',
                        ext: '.css'
                    }
                ]
            }
        },
        copy: {
            images: {
                expand: true,
                cwd: 'web/assets/src/admin/images',
                src: '*',
                dest: '<%= distFolder %>/images/'
            },
            images_front: {
                expand: true,
                cwd: 'web/assets/src/front/images',
                src: '*',
                dest: '<%= distFolder %>/front/images/'
            }
        },
        watch: {
            scripts: {
                files: [
                    'web/assets/src/admin/js/*.js',
                    'web/assets/src/admn/scss/**/*.scss',
                    'web/assets/src/front/js/*.js',
                    'web/assets/src/front/scss/**/*.scss'
                ],
                tasks: ['dev'],
                options: {
                    spawn: true
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['copy', 'sass', 'concat', 'cssmin', 'uglify']);
    grunt.registerTask('dev', ['copy', 'sass', 'concat']);
};
<?php
return [
    'Password'        => 'Heslo' ,
    'Repeat Password' => 'Zopakuj heslo' ,
    'actions'         => 'Akcie' ,

    'admin' => [
        'config' => [
            'list' => 'Systémové nastavenia' ,
        ] ,

        'locale' => [
            'list' => 'Jazyky' ,
        ] ,
    ] ,

    'back' => 'Naspäť' ,

    'blog' => [
        'article' => [
            'created'     => 'Článok bol vytvorený' ,
            'delete'      => 'Ste si istý' ,
            'deleted'     => 'Článok bol vymazaný' ,
            'not_allowed' => 'Nemáte oprávnenie zmazať článok' ,
            'read_more'   => 'Viac' ,
            'updated'     => 'Článok bol aktualizovaný' ,
        ] ,

        'article_edit'         => 'Uprav článok' ,
        'article_new'          => 'Napíš článok' ,
        'articles'             => 'Články' ,
        'articles.all'         => 'Všetky moje články' ,
        'articles.no_articles' => 'Pridajte svôj prvý článok' ,
        'articles.welcome'     => 'Pustime sa do toho..' ,
        'articles_latest'      => 'Posledné články' ,

        'button' => [
            'join' => 'Pridajte sa k nám' ,
        ] ,

        'form' => [
            'article' => [
                'body'              => 'Obsah' ,
                'enable_discussion' => 'Povoliť diskusiu' ,
                'image'             => 'Obrázok' ,
                'locale'            => 'Jazyk' ,
                'save'              => 'Ulož' ,
                'tags'              => 'Tagy' ,
                'title'             => 'Nadpis' ,
            ] ,
        ] ,

        'join_us' => 'Začnite písať aj Vy' ,
    ] ,

    'button' => [
        'user' => [
            'register' => 'Registrujte sa' ,
        ] ,
    ] ,

    'cancel'    => 'Zrušiť' ,
    'create'    => 'Vytvor' ,
    'dashboard' => 'Dashboard' ,
    'default'   => 'Default' ,
    'delete'    => 'Zmazať' ,
    'edit'      => 'Upraviť' ,

    'email' => [
        'subject' => [
            'user_registration' => 'Registrácia používateľa - Igsem' ,
        ] ,
    ] ,

    'featured' => [
        'authors'  => 'Autori' ,
        'featured' => 'Odporúčame' ,
        'popular'  => 'Populárne' ,
        'tags'     => 'Tagy' ,
    ] ,

    'form' => [
        'user' => [
            'email'           => 'Email' ,
            'name'            => 'Meno' ,
            'nick'            => 'Nick' ,
            'password'        => 'Heslo' ,
            'password_repeat' => 'Overenie hesla' ,
            'surname'         => 'Priezvisko' ,
        ] ,
    ] ,

    'front' => [
        'comming_soon' => 'Stránka sa pripravuje...' ,
        'visit_blog'   => 'Navštívte náš Blog' ,
    ] ,

    'links' => [
        'about'    => 'O nás' ,
        'blog'     => 'Blog' ,
        'homepage' => 'Domov' ,
    ] ,

    'locale'         => 'Jayzk' ,
    'login'          => 'Prihlásenie' ,
    'login.password' => 'Heslo' ,
    'login.username' => 'Nick' ,
    'logout'         => 'Odhlásenie' ,
    'new'            => 'Nový' ,

    'register' => [
        'join_us'         => 'Staň sa aj ty autorom článkov.' ,
        'join_us.message' => 'Ak by si chcel zdielať svoje vedomosti a skusenosti, vyplň tento formulár a do 24 hodín získaš prístup.' ,
    ] ,

    'show' => 'Zobraz' ,

    'sidebar' => [
        'locale'      => 'Jazyk' ,
        'site_config' => 'Nastavenie stránky' ,
    ] ,

    'title' => 'Nadpis' ,
];

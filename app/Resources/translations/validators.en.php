<?php
return array(
    'The password fields must match' => array(
        '' => 'The password fields must match.',
    ),

    'blog' => array(
        'article' => array(
            'not_blank' => array(
                'body' => 'Body can not be empty',
                'locale' => 'Language can not be empty',
                'title' => 'Title can not be empty',
            ),
        ),

        'site_config' => array(
            'not_blank' => array(
                'locale' => 'Locale can not be blank',
            ),
        ),
    ),
);

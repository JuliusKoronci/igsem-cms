<?php
return array(
    'Password' => 'Password',
    'Repeat Password' => 'Repeat Password',
    'actions' => 'Actions',

    'admin' => array(
        'config' => array(
            'edit' => 'Edit Configuration',
            'list' => 'SITE Configuration',
        ),

        'locale' => array(
            'list' => 'List of Locales',
        ),
    ),

    'back' => 'Back',

    'blog' => array(
        'article' => array(
            'created' => 'Article was created',
            'delete' => 'Are you sure?',
            'deleted' => 'Article was deleted',
            'not_allowed' => 'You are not allowed to delete this article!',
            'read_more' => 'Read more',
            'updated' => 'Your Article was updated',
        ),

        'article_edit' => 'Edit your article',
        'article_new' => 'Create a new Article',
        'articles' => 'Articles',
        'articles.all' => 'All my articles',
        'articles.no_articles' => 'You are yet to add an article!',
        'articles.welcome' => 'Let\'s get started!',
        'articles_latest' => 'Latest articles',

        'button' => array(
            'join' => 'JOIN US',
        ),

        'form' => array(
            'article' => array(
                'body' => 'Body',
                'enable_discussion' => 'Enable Discussion',
                'featured' => 'Featured',
                'image' => 'Cover image',
                'locale' => 'Language',
                'main' => 'Main Article',
                'save' => 'Save',
                'tags' => 'Tags',
                'title' => 'Title',
            ),
        ),

        'join_us' => 'Anyone interested in sharing their knowledge and experience can join us and post articles :)',
    ),

    'button' => array(
        'user' => array(
            'register' => 'Register',
        ),
    ),

    'cancel' => 'Cancel',
    'create' => 'Create',
    'dashboard' => 'Dashboard',
    'default' => 'Default',
    'delete' => 'Delete',
    'edit' => 'Edit',

    'email' => array(
        'subject' => array(
            'user_registration' => 'User registration at Igsem',
        ),
    ),

    'featured' => array(
        'authors' => 'Authors',
        'featured' => 'Featured',
        'popular' => 'Popular',
        'tags' => 'Tags',
    ),

    'form' => array(
        'config' => array(
            'save' => 'Save',
        ),

        'user' => array(
            'email' => 'Email',
            'name' => 'Name',
            'nick' => 'Nickname',
            'password' => 'Password',
            'password_repeat' => 'Repeat password',
            'surname' => 'Surname',
        ),
    ),

    'front' => array(
        'comming_soon' => 'Our main page is coming soon :)',
        'visit_blog' => 'Check out our Blog',
    ),

    'links' => array(
        'about' => 'About us',
        'blog' => 'Blog',
        'homepage' => 'Homepage',
    ),

    'locale' => 'Locale',
    'login' => 'Login',
    'login.password' => 'Password',
    'login.username' => 'Username',
    'logout' => 'Logout',
    'new' => 'New',

    'register' => array(
        'join_us' => 'Become an  Author at Igsem',
        'join_us.message' => 'If you wish to share your knowledge and opinions, fill out this form and we will grant you access within 24 hours!',
    ),

    'show' => 'Show',

    'sidebar' => array(
        'locale' => 'Locale',
        'site_config' => 'Site config',
    ),

    'title' => 'Title',
);

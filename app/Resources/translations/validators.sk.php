<?php
return array(
    'The password fields must match' => array(
        '' => 'The password fields must match.',
    ),

    'blog' => array(
        'article' => array(
            'not_blank' => array(
                'body' => 'blog.article.not_blank.body',
                'locale' => 'blog.article.not_blank.locale',
                'title' => 'blog.article.not_blank.title',
            ),
        ),

        'site_config' => array(
            'not_blank' => array(
                'locale' => 'blog.site_config.not_blank.locale',
            ),
        ),
    ),
);
